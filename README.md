# go-pi-irrigation

Irrigation API using Golang for Raspberry Pi

## Install Instructions
```
git clone https://gitlab.com/devonrjohnson/go-pi-irrigation
cd go-pi-irrigation
export GOPATH=$(pwd)
go get ./...
go install go-irrigate
./install.sh
```

Then start the service

```
systemctl start go-irrigate
```

## Build info
This is made to be run on a Raspberry Pi. It is hard coded to use GPIO pins 11, 13, 15, 29, 31, 33, 35, and 37 for relay controls.

## Example Endpoint use

```
curl -X POST http://address:8000/run/1 --data '{ "seconds": 30 }'
curl http://address:8000/toggle/1
curl http://address:8000/status
```

