package main

import "github.com/stianeikeland/go-rpio"

type valve struct{
    Location string
    Pin rpio.Pin
    Channel int
}

type timer struct{
    Seconds int
}

type status struct{
    Status string `json:"Status"`
    Location string `json:"Location"`
    Channel int `json:"Channel"`
}

//type statStruc struct {
//    Main []interface{} `json:"main"`
//    Stat string `json:"stat"`
//}
