package main

import (
    "net/http"
    "fmt"
    "io"
    "encoding/json"
    "strings"
    "strconv"
)

func On(w http.ResponseWriter, r *http.Request) {
    v := ValveSelect()
    if v == nil {
        w.WriteHeader(400)
        return
    } else {
        v.Pin.Low()
        fmt.Fprintln(w, "valve " + v.Location + " turned on")
    }
}

func Off(w http.ResponseWriter, r *http.Request) {
    v := ValveSelect()
    if v == nil {
        w.WriteHeader(400)
        return
    } else {
        v.Pin.High()
        fmt.Fprintln(w, "valve " + v.Location + " turned off")
    }
}


func Toggle(w http.ResponseWriter, r *http.Request) {
    body := GetBody(r)
    v := ValveSelect()
    var onOrOff bool;
    json.Unmarshal(body, &onOrOff)
    jsonData := struct{ Active bool `json:"active"`}{}
    if v == nil {
        w.WriteHeader(400)
        return
    }
    //If the body is empty, perform toggle, if true, turn on, if false, turn off 
    //Done to conform to HomeAssistant Restful Switch Entity
    if string(body) == "" {
	if v.Pin.Read() == 1 {
	    if r.Method == "POST" { v.Pin.Low()
	    } else { jsonData.Active = false }
	} else {
	    if r.Method == "POST" { v.Pin.High()
	    } else { jsonData.Active = true }
	}
    } else if onOrOff == true {
	v.Pin.Low()
    } else if onOrOff == false {
	v.Pin.High()
    } else {
	panic("Not valid input for Toggle")
    }

    if r.Method == "GET" {json.NewEncoder(w).Encode(jsonData)}
}

func RunTime(w http.ResponseWriter, r *http.Request) {
    body := GetBody(r)
    v := ValveSelect()
    t := timer{}

    json.Unmarshal(body, &t)
    v.Pin.Low()
    go Timer(t.Seconds, v)

}

func Edit(w http.ResponseWriter, r *http.Request) {
    body := GetBody(r)
    vNew := valve{}
    json.Unmarshal(body, &vNew)
    v := ValveSelect()
    if v == nil {
	w.WriteHeader(400)
	return
    }
    query := "UPDATE valves SET location=?  WHERE channel=?"
    statement, _ := db.Prepare(query)
    statement.Exec(vNew.Location,  v.Channel)
    w.WriteHeader(200)
}

func Create(w http.ResponseWriter, r *http.Request) {
    body := GetBody(r)
    v := valve{}
    err := json.Unmarshal(body, &v)
    v.Channel = channel
    if err != nil {
        w.Header().Set("Content-Type", "application/json; charset=UTF-8")
        w.WriteHeader(422)
        if err := json.NewEncoder(w).Encode(err); err != nil {
            panic(err)
        }
    }

    if v.Channel < 1 || v.Channel > 8 {
        w.WriteHeader(422)
        return
    }
    if ValveSelect() != nil {
        w.WriteHeader(409)
        return
    }

    query := "INSERT INTO valves (location, pin, channel) values (?, ?, ?)"
    statement, _ := db.Prepare(query)
    statement.Exec(v.Location, pins[v.Channel - 1], v.Channel)
    w.WriteHeader(201)
}

func List(w http.ResponseWriter, r *http.Request) {
    rows, _ := db.Query("SELECT id, location, pin FROM valves")
    v := valve{}
    var id int
    for rows.Next() {
	rows.Scan(&id, &v.Location, &v.Pin)
	json.NewEncoder(w).Encode(v)
    }
}

func Status(w http.ResponseWriter, r *http.Request) {
	rows, _ := db.Query("SELECT pin, location, channel FROM valves")
	v := valve{}
	jsonOutput := struct {
	    Main []interface{} `json:"main"`
	    Stat string `json:"stat"`
	}{Stat: "OK"}
	s := status{}
	for rows.Next() {
	    rows.Scan(&v.Pin,&v.Location,&v.Channel)

	    s = status{ Location: v.Location, Channel: v.Channel }

	    state := v.Pin.Read()

	    switch state {
		case 0: //Low
		    s.Status = "Running"
		case 1: //High
		    s.Status = "Off"
		default:
		    panic("Undefined state: " + string(state))
	    }
	    jsonOutput.Main = append(jsonOutput.Main, s)
	}
	jsonOutput.Stat = "OK"
	json.NewEncoder(w).Encode(jsonOutput)
}

var channel int

func (*myHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
    var URLString string
    arr := strings.Split(r.URL.String(),"/")
    if len(arr) > 2 {
	channel, _ = strconv.Atoi(arr[2])
    }
    URLString = arr[1]
    if h,ok := mux[URLString]; ok{
        h(w, r)
        return
    }

    io.WriteString(w, "My server: "+r.URL.String()+"\n")
}
