package main

import (
    "fmt"
    "io/ioutil"
    "io"
    "os"
    "time"
    "net/http"
    "github.com/stianeikeland/go-rpio"
    "database/sql"
    _ "github.com/mattn/go-sqlite3"

)

func ValveSelect() (*valve) {
    v := valve{}
    query := fmt.Sprintf("SELECT location,pin,channel FROM valves where channel=%d", channel)
    row := db.QueryRow(query)

    var location string
    var pin int
    var vChannel int

    err = row.Scan(&location,&pin,&vChannel)
    if err != nil {
        return nil
    }
    v = valve{Location:location, Pin:rpio.Pin(pin), Channel: channel}
    return &v
}

func GetBody(r *http.Request) ([]byte) {
    body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
    if err != nil { panic(err) }
    return body
}

func init_db() {
    if _, err := os.Stat("./sqlite3.db"); err != nil{
        database, _ := sql.Open("sqlite3", "./sqlite3.db")
        fmt.Println("file opened")
        statement, _ := database.Prepare("CREATE TABLE IF NOT EXISTS valves (id INTEGER PRIMARY KEY, location TEXT, pin INTEGER, channel INTEGER, CONSTRAINT channel_unique UNIQUE (channel, pin))")
        statement.Exec()
        fmt.Println("file edited")
    }
}


type myHandler struct{}

func init_server()(*http.Server) {
	server := http.Server{
		Addr: ":8000",
		Handler: &myHandler{},
	}
	return &server
}

func init_rpio() {
	var pin rpio.Pin
	for _, i := range pins {
		pin = rpio.Pin(i)
		fmt.Printf("Testing Pin %d\n", pin)
		pin.Output()
	}
}


func Timer(seconds int, v *valve) {
    time.Sleep(time.Duration(seconds) * time.Second)
    v.Pin.High()
    fmt.Println("Valve " + v.Location + " turned off.")
}


var mux map[string]func(http.ResponseWriter, *http.Request)

func CreateEndpoints(s *http.Server) {
    mux = make(map[string]func(http.ResponseWriter, *http.Request))
    mux["on"] = On
    mux["off"] = Off
    mux["toggle"] = Toggle
    mux["create"] = Create
    mux["edit"] = Edit
    mux["list"] = List
    mux["run"] = RunTime
    mux["status"] = Status
}
