package main

import (
 "database/sql"
 "github.com/stianeikeland/go-rpio"
 _ "github.com/mattn/go-sqlite3"
 )



var pins = [...]int{17,27,22,5,6,13,19,26}
var db, _ = sql.Open("sqlite3", "./sqlite3.db")
var err = rpio.Open()

func main() {
    init_db()
    init_rpio()
    server := init_server()
    CreateEndpoints(server)
    server.ListenAndServe()
    rpio.Close()
}


