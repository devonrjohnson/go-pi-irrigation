#!/bin/bash

if [[ $UID != 0 ]]
then
    echo "Must be run with root privileges."
    exit 1
fi

export GOPATH=$(pwd)
go install go-irrigate
mv go-irrigate /usr/local/bin/
cp go-irrigate.service /etc/systemd/system/
systemctl daemon-reload
mkdir /usr/share/go-irrigate
